import { Actor } from "./Actor";
import { IContentToWatch } from "./IContentToWatch";
import { Worker } from "./Worker";
import { Critique } from "./Critique";

export class TvSeries implements IContentToWatch {

    constructor(public name: string, public director: Worker, public workers: Worker[],
        public actors: Actor[], public releaseDate: Date, public critiques: Critique[]) { }

    AddWorker(worker: Worker): void {
        this.workers.push(worker);
    }
    RemoveWorkerById(id: string): void {
        this.workers.filter((worker) => { return worker.id !== id });
    }
    AddCritique(critique: Critique): void {
        this.critiques.push(critique);
    }

}