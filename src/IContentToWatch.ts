import { Actor } from "./Actor";
import { Worker } from "./Worker";
import { Critique } from "./Critique";

export interface IContentToWatch {
     name: string,
     director: Worker,
     workers: Worker[],
     actors: Actor[],
     releaseDate: Date,
     critiques: Critique[]

     AddWorker(worker: Worker): void
     RemoveWorkerById(id: string): void //ולידצית קביעות עובד
     AddCritique(critique: Critique): void
}