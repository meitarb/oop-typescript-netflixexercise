export enum WorkerJobs {
    Director,
    Cameraman,
    Editor,
    Soundman
}