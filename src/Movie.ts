import { Actor } from "./Actor";
import { IContentToWatch } from "./IContentToWatch";
import { Worker } from "./Worker";
import { Critique } from "./Critique";
import _, { isArrayLikeObject } from "lodash";
import { WorkerJobs } from "./WorkerJobs";

const YearsMillisecond = 31536000000;

export class Movie implements IContentToWatch {

    public name: string;
    public director: Worker;
    public workers: Worker[];
    public actors: Actor[];
    public releaseDate: Date;
    public critiques: Critique[];

    constructor(name: string, director: Worker, workers: Worker[],
        actors: Actor[], realeseDate: Date, critiques: Critique[]) {
        if (this.EditorValidetion(workers) > 1 || this.SoundmanValidetion(workers) > 2) {
            throw new Error("In movie there must be only one Director and meximum 2 soundmans");
        } else {
            this.name = name;
            this.director = director;
            this.workers = workers;
            this.actors = actors;
            this.releaseDate = realeseDate;
            this.critiques = new Array<Critique>();

        }
    }

    AddWorker(worker: Worker): void {
        if (this.EditorValidetion(this.workers) > 1 && worker.job === WorkerJobs.Editor
            || this.SoundmanValidetion(this.workers) > 2 && worker.job === WorkerJobs.Soundman) {
            throw new Error("In movie there must be only one Director and meximum 2 soundmans");
        } else {
            this.workers.push(worker);
        }
    }
    RemoveWorkerById(id: string): void {
        this.workers = this.workers.filter((worker) => {
            return !(worker.id === id &&
                worker.workHistory.some(element => element.name === this.name &&
                    (Date.now() - element.joiningDate.getDate()) / YearsMillisecond < 2));
        });
    }

    AddCritique(critique: Critique): void {
        this.critiques.push(critique);
    }

    EditorValidetion(workers: Worker[]): number {
        return workers.filter(worker => worker.job === WorkerJobs.Editor).length;
    }

    SoundmanValidetion(workers: Worker[]): number {
        return workers.filter(worker => worker.job === WorkerJobs.Soundman).length;
    }

    MovieAverageRate(): number {
        return this.critiques.reduce((sum, cur): number => {
            return sum + cur.rate;
        }, 0) / this.critiques.length;
    }

}