import { User } from "./User";

export class Critique {
    private _rate: number;

    constructor(private _user: User, rate: number, private _content: string) {
        if (!this.checkRate(rate)) {
            throw new Error("rate must be between 1 to 5");
        } else {
            this._rate = rate;
        }
    }

    public get rate(): number {
        return this._rate;
    }

    public set rate(newRate: number) {
        if (!this.checkRate(newRate)) {
            throw new Error("rate must be between 1 to 5");
        } else {
            this._rate = newRate;
        }
    }

    public get content(): string {
        return this._content;
    }
    public set content(value: string) {
        this._content = value;
    }

    public get user(): User {
        return this._user;
    }
    public set user(value: User) {
        this._user = value;
    }

    checkRate = (rate: number) => (rate >= 1 && rate <= 5);
}