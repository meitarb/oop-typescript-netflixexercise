import { Gender } from "./Gender";
import { Person } from "./Person";
import { WorkerJobs } from "./WorkerJobs";
import { IContentToWatch } from "./IContentToWatch";
import _ from "lodash";

export class Worker extends Person {
    protected _job: WorkerJobs;
    protected _workHistory: (IContentToWatch & { joiningDate: Date })[]; // movie\series and joining date

    constructor(job: WorkerJobs, workHistory: (IContentToWatch & { joiningDate: Date })[] = [],
        firstName: string, lastName: string, gender: Gender, age: number, id: string, birthday: Date) {

        let isHistoryInValid: boolean = workHistory.some(element => {
            return Date.now() - element.joiningDate.getDate() < 0
        });

        if (!isHistoryInValid) {
            super(firstName, lastName, gender, age, id, birthday);
            this._job = job;
            this._workHistory = workHistory;
            this._workHistory.keys
        } else {
            throw new Error("joining date must be valid and Equal to or less than the date of today");
        }
    }

    public get job(): WorkerJobs {
        return this.job;
    }

    public set job(newJob: WorkerJobs) {
        this.job = newJob;
    }

    public get workHistory(): (IContentToWatch & { joiningDate: Date })[] {
        return this.workHistory;
    }

    public AddWork(content: IContentToWatch, joiningDate: Date) {
        if (joiningDate.getDate <= Date.now) {
            this._workHistory.push({ joiningDate, ...content });
        } else {
            throw new Error("joining date must be valid and Equal to or less than the date of today");
        }

    }
}