import { Actor } from "./Actor";
import { User } from "./User";
import { Worker } from "./Worker";
import { Movie } from "./Movie";
import { TvSeries } from "./TvSeries";
import { IContentToWatch } from "./IContentToWatch";
import { ActorJobs } from "./ActorJobs";
import { Person } from "./Person";

export class Netflix {

    constructor(public movies: Movie[], public tvSeries: TvSeries[], public users: User[]) {
    }

    AddMovie(movie: Movie): void {
        this.movies.push(movie);
    }

    ContentExists(content: IContentToWatch): boolean {
        return this.movies.some(movie => movie === content) || this.tvSeries.some(series => series === content);
    }

    UserExists(username: string): boolean {
        return this.users.some(user => user.userName === username);
    }

    OldestMovie(movieName: string): Movie {
        const sameMovies = this.movies.filter(movie => movie.name === movieName);
        return sameMovies.reduce((a, b): Movie => {
            return a.releaseDate < b.releaseDate ? a : b
        })
    }

    RemoveContent(contentName: string): void {
        this.movies = this.movies.filter(movie => movie.name !== contentName);
        this.tvSeries = this.tvSeries.filter(series => series.name !== contentName);
    }

    AddWorker(worker: Worker, content: IContentToWatch): void {
        if (this.ContentExists(content)) {
            content.AddWorker(worker);
        } else {
            throw new Error("this content doesnt exist in netflix. please add it first and then add the worker!");
        }
    }

    RemoveWorkerById(id: string, content: IContentToWatch): void {
        if (this.ContentExists(content)) {
            content.RemoveWorkerById(id);
        }
        else {
            throw new Error("this content doesnt exist in netflix. please add it first and then remove the worker!");
        }
    }

    AddUser(newUser: User): void {
        if (this.UserExists(newUser.userName)) {
            throw new Error("this username already exsits!");
        } else {
            this.users.push(newUser);
        }
    }

    RemoveUserById(id: string): void {
        this.users.filter(user => user.id !== id);
    }

    RemoveUserByUsername(username: string) {
        this.users.filter(user => user.userName !== username);
    }

    EditNickname(newUsername: string, user: User): void {
        if (this.UserExists(newUsername)) {
            throw new Error("this username already exsits!");
        } else {
            user.userName = newUsername;
        }
    }

    MainHeroOfMovie(movieName: string): Actor[] {
        return this.OldestMovie(movieName)
            .actors.filter(actor => actor.actorJob === ActorJobs.main_hero);
    }

    AvarageMovieRate(movieName: string): number {
        return this.OldestMovie(movieName).MovieAverageRate();
    }

    NewMoviesFromDate(date: Date): { _name: string, _realeseDate: Date }[] {
        return this.movies.filter(movie => movie.releaseDate >= date).sort((a: Movie, b: Movie) => {
            return this.getTime(a.releaseDate) - this.getTime(b.releaseDate);
        }).map(({ name: _name, releaseDate: _realeseDate }) => ({ _name, _realeseDate }));
    }

    private getTime(date?: Date) {
        return date != null ? date.getTime() : 0;
    }

    AgeAndBirthdayVaidation(person: Person): boolean {
        const timeDiff = Math.abs(Date.now() - person.birthday.getTime());
        const age = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365.25);
        return person.age === age;
    }
}