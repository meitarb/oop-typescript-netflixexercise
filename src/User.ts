import { Gender } from "./Gender";
import { Person } from "./Person";

export class User extends Person {

    protected _userName: string;

    constructor(userName: string,
        firstName: string, lastName: string, gender: Gender, age: number, id: string, birthday: Date) {
        super(firstName, lastName, gender, age, id, birthday);
        if (this.checkUsername(userName)) {
            throw new Error("username must be 10 characters maximum!");
        }
        else {
            this._userName = userName;
        }
    }

    public get userName(): string {
        return this._userName;
    }

    public set userName(newUsername: string) {
        if (this.checkUsername(newUsername)) {
            throw new Error("username must be 10 characters maximum!");
        }
        else {
            this._userName = newUsername;
        }
    }

    checkUsername = (nickname: string) => (nickname.length < 10);
}

