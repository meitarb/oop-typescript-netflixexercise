import { Gender } from "./Gender";
import { Person } from "./Person";
import { ActorJobs } from "./ActorJobs";

export class Actor extends Person {

    constructor(protected _actorJob: ActorJobs,
        firstName: string, lastName: string, gender: Gender, age: number, id: string, birthday: Date) {
        super(firstName, lastName, gender, age, id, birthday);
    }

    public get actorJob(): ActorJobs {
        return this.actorJob;
    }

    public set actorJob(newJob: ActorJobs) {
        this._actorJob = newJob;
    }
}