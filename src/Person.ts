import { Gender } from './Gender';

export abstract class Person {

    protected _firstName: string;
    protected _lastName: string;
    protected _gender: Gender;
    protected _age: number;
    protected _id: string; //can't change after decleration
    protected _birthday: Date; //can't change after decleration

    constructor(firstName: string, lastName: string, gender: Gender, age: number, id: string, birthday: Date) {
        if (id.length != 9 || age < 0 || age > 200 || birthday.getDate > Date.now) {
            throw new Error("The id is invalid, it must be with 9 characters only");
        }
        else {
            this._firstName = firstName;
            this._lastName = lastName;
            this._gender = gender;
            this._age = age;
            this._id = id;
            this._birthday = birthday;
        }
    }

    public get firstName(): string {
        return this._firstName;
    }

    public set firstName(newFirstName: string) {
        this._firstName = newFirstName;
    }

    public get lastName(): string {
        return this._lastName;
    }

    public set lastName(newLastName: string) {
        this._lastName = newLastName;
    }

    public get gender(): Gender {
        return this._gender;
    }

    public set gender(newGender: Gender) {
        this._gender = newGender;
    }


    public get age() {
        return this.age;
    }

    public set age(age: number) {
        if (age <= 0 || age >= 200) {
            throw new Error('The age is invalid');
        }
        this.age = age;
    }

    public get id() {
        return this._id;
    }

    public get birthday() {
        return this._birthday;
    }
}